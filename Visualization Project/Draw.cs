﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;
using Visualization_Project;

namespace Visualization_Project

{
    public class Draw
    {
        public Canvas canvas;
        public List<Coordinate> coordinates;
        public double minX = 0;
        public double maxX = 0;
        public double minY = 0;
        public double maxY = 0;

        public Draw(Canvas canvas)
        {
            this.canvas = canvas;
            coordinates = new List<Coordinate>();
        }
        public void setLimits(double minimumX, double minimumY, double maximumX, double maximumY) {
            minX = minimumX;
            minY = minimumY;
            maxX = maximumX;
            maxY = maximumY;

        }
        public void AddCoordinate(Coordinate coordinate)
        {
            coordinates.Add(coordinate);
            DrawPoint(coordinate.X, coordinate.Y);
        }

        private void DrawPoint(double x, double y)
        {
            Ellipse point = new Ellipse
            {
                Width = 4,
                Height = 4,
                Fill = Brushes.Blue,
                Stroke = Brushes.Black,
                StrokeThickness = 1
            };

            Canvas.SetLeft(point, x - point.Width / 2);
            Canvas.SetTop(point, y - point.Height / 2);

            canvas.Children.Add(point);
        }

        public void ConnectPoints()
        {
            for (int i = 1; i < coordinates.Count; i++)
            {
                Point startPoint = new Point(coordinates[i - 1].X, coordinates[i - 1].Y);
                Point endPoint = new Point(coordinates[i].X, coordinates[i].Y);

                Line line = new Line
                {
                    X1 = startPoint.X,
                    Y1 = startPoint.Y,
                    X2 = endPoint.X,
                    Y2 = endPoint.Y,
                    Stroke = Brushes.Black,
                    StrokeThickness = 1
                };

                canvas.Children.Add(line);
            }
        }
        public Coordinate ScaleCoordinate(Coordinate coordinate)
        {
            double canvasWidth = canvas.ActualWidth;
            double canvasHeight = canvas.ActualHeight;

            if (maxX - minX == 0 || maxY - minY == 0)
            {
                Debug.WriteLine("Error: Cannot scale coordinates, division by zero.");
                return new Coordinate(coordinate.X, coordinate.Y);
            }
            double scaledX = (coordinate.X - minX) / (maxX - minX) * canvasWidth;
            double scaledY = (coordinate.Y - minY) / (maxY - minY) * canvasHeight;
            Coordinate scaled = new Coordinate(scaledX, scaledY);
            return scaled;
        }



        public async Task ConnectTwoPoints(Coordinate coordinateA, Coordinate coordinateB)
        {
            Debug.WriteLine("Draw called");
            Coordinate scaledA = ScaleCoordinate(coordinateA);
            Coordinate scaledB = ScaleCoordinate(coordinateB);
            //Debug.WriteLine("A:" + scaledA + "||||" + coordinateA);
            //Debug.WriteLine("B:" + scaledB + "||||" + coordinateB);
            Point startPoint = new Point(scaledA.X, scaledA.Y);
            Point endPoint = new Point(scaledB.X, scaledB.Y);

            Line line = new Line
            {
                X1 = startPoint.X,
                Y1 = startPoint.Y,
                X2 = endPoint.X,
                Y2 = endPoint.Y,
                Stroke = Brushes.Black,
                StrokeThickness = 1
            };
            
            if (!Application.Current.Dispatcher.CheckAccess())
            {
                await Application.Current.Dispatcher.InvokeAsync(() => canvas.Children.Add(line));
            }
            else
            {
                canvas.Children.Add(line);
                Debug.WriteLine("Draw called");
            }
        }

        public void ClearCanvas()
        {
            canvas.Children.Clear();
            coordinates.Clear();
        }

        public void ClearLines()
        {
            foreach (UIElement element in canvas.Children.OfType<Line>().ToList())
            {
                canvas.Children.Remove(element);
            }
        }


    }
}
