﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;
using Visualization_Project;

namespace Visualization_Project
{
    public class AngleTSPSolver
    {
        private Hole[] holes;
        private int[] order;
        private Random random = new Random();
        private double firstDistance;
        private int maxIterations;
        private double angleWeight;
        private MainWindow mainWindow;

        public AngleTSPSolver(Hole[] holes, int maxIterations = 10000, double angleWeight = 1.0)
        {
            this.holes = holes;
            this.maxIterations = maxIterations;
            this.angleWeight = angleWeight;
            InitializeOrder();
        }
        public AngleTSPSolver(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
        }

        public void setValues(Hole[] holes, int maxIterations = 10000, double angleWeight = 1.0) {
            this.holes = holes;
            this.maxIterations = maxIterations;
            this.angleWeight = angleWeight;
            InitializeOrder();
        }
        private void InitializeOrder()
        {
            int N = holes.Length;
            order = new int[N];
            for (int i = 0; i < N; i++)
            {
                order[i] = i;
            }
        }

        public void Solve()
        {
            int iteration = 0;
            bool noChange = true;
            firstDistance = CalculateTotalDistance();

            while (iteration < maxIterations && noChange)
            {
                OptimizeRoute();
                double currentDistance = CalculateTotalDistance();

                noChange = currentDistance >= firstDistance;
                if (!noChange)
                {
                    firstDistance = currentDistance;
                }

                iteration++;
            }
        }

        private double CalculateTotalDistance()
        {
            double totalDistance = 0;
            double lastAngle = 0;
            for (int i = 0; i < order.Length - 1; i++)
            {
                totalDistance += Distance(holes[order[i]], holes[order[i + 1]]);

                Debug.WriteLine("1: " + i);
                double newAngle = CalculateAngle(i);
                totalDistance += angleWeight * Math.Abs(newAngle - lastAngle);
                lastAngle = newAngle;
            }
            return totalDistance;
        }

        private async Task ConnectTwoPoints(Coordinate coordinateA, Coordinate coordinateB) 
        {
            await mainWindow.ConnectTwoPoints(coordinateA, coordinateB);
        }
        private double Distance(Hole a, Hole b)
        {
            return Math.Sqrt(Math.Pow(a.X - b.X, 2) + Math.Pow(a.Y - b.Y, 2));
        }
        
        private double CalculateAngle(int index)
        {
            int prevIndex = index == 0 ? order.Length - 1 : index - 1;
            //Debug.WriteLine("prevIndex: " + prevIndex);
            int holeIndex = order[prevIndex];

            int nextIndex = (index + 1) % order.Length;
            Hole prev = holes[order[prevIndex]];
            Hole current = holes[order[index]];
            Hole next = holes[order[nextIndex]];

            Coordinate coordinateA = new Coordinate(holes[order[index]].X, holes[order[nextIndex]].Y);
            Coordinate coordinateB = new Coordinate(holes[order[nextIndex]].X, holes[order[nextIndex]].Y);

            //Issue does not draw, Threading 

            _ = ConnectTwoPoints(coordinateA, coordinateB);

            double angle1 = Math.Atan2(current.Y - prev.Y, current.X - prev.X);
            double angle2 = Math.Atan2(next.Y - current.Y, next.X - current.X);
            return Math.Abs(angle2 - angle1);
        }

        private void OptimizeRoute()
        {
            int N = holes.Length;
            int flip = random.Next(N - 3) + 2;

            for (int i = 0; i < N - 1; i++)
            {
                for (int j = i + flip; j < N; j++)
                {
                    // Calculate the total distance if we were to flip the path between cities[i] and cities[j]
                    double flippedDistance = CalculateFlippedDistance(i, j);
                    double currentDistance = CalculateSegmentDistance(i, j);

                    if (flippedDistance < currentDistance)
                    {
                        // Perform the flip if it reduces the distance
                        Array.Reverse(order, i, j - i + 1);
                    }
                }
            }
        }

        private double CalculateFlippedDistance(int start, int end)
        {
            double distance = 0;
            //Issue is here if start = 0 it will be sent as -1 which is not handled by CalculateAngle(). Unsure how it should be handled
            Debug.WriteLine("2: " + (start - 1));
            /* ==============================
             Change this to correct fix or comment out to see error */

            if (start == 0) { start += 1; }

            //================================
            double lastAngle = CalculateAngle(start - 1);

            for (int i = start; i <= end; i++)
            {
                int nextIndex = i == end ? start : i + 1;
                //Debug.WriteLine(holes[order[i]]);
                distance += Distance(holes[order[i]], holes[order[nextIndex]]);
                Debug.WriteLine("3: " + i);
                double newAngle = CalculateAngle(i);
                distance += angleWeight * Math.Abs(newAngle - lastAngle);
                lastAngle = newAngle;
            }

            return distance;
        }

        private double CalculateSegmentDistance(int start, int end)
        {
            double distance = 0;
            Debug.WriteLine("4: " + (start - 1));
            /* ==============================
             Change this to correct fix or comment out to see error */

            if (start == 0) { start += 1; }

            //================================ 
            double lastAngle = CalculateAngle(start - 1);

            for (int i = start; i < end; i++)
            {
                distance += Distance(holes[order[i]], holes[order[i + 1]]);
                Debug.WriteLine("5: " + i);
                double newAngle = CalculateAngle(i);
                distance += angleWeight * Math.Abs(newAngle - lastAngle);
                lastAngle = newAngle;
            }

            return distance;
        }
    }
}
