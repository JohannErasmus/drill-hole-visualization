﻿using System.Diagnostics;
using System.Formats.Asn1;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;


namespace Visualization_Project
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Draw draw;
        private List<Hole> holes = new List<Hole>();
        private AngleTSPSolver solver;
        public List<Coordinate> loadedCoordinates = new List<Coordinate>();
        private Hole[] holesA ;
        public double minX = 0;
        public double maxX = 0;
        public double minY = 0;
        public double maxY = 0;

        public MainWindow()
        {
            InitializeComponent();
            WindowState = WindowState.Maximized;

            draw = new Draw(coordinateCanvas);
            solver = new AngleTSPSolver(this);

        }

        private void SelectFile_Click(object sender, RoutedEventArgs e)
        {
            draw.ClearCanvas();

            string filePath = FileExplorer.SelectCsvFile();
            
            if (filePath != null)
            {
                loadedCoordinates = FileReader.ReadCoordinates(filePath);
                minX = loadedCoordinates.Min(c => c.X);
                maxX = loadedCoordinates.Max(c => c.X);
                minY = loadedCoordinates.Min(c => c.Y);
                maxY = loadedCoordinates.Max(c => c.Y);
                draw.setLimits(minX, minY, maxX, maxY);
                foreach (Coordinate coordinate in loadedCoordinates)
                {
                    Coordinate scaled = draw.ScaleCoordinate(coordinate);
                    
                    draw.AddCoordinate(scaled);
                    holes.Add(new Hole(coordinate.X, coordinate.Y));
                }
                infoText.Text = $"Coordinates loaded from: {filePath}";
            }
            else
            {
                infoText.Text = "File selection canceled.";
            }
        }

        private void TSPSolver(List<Hole> holes) {
            holesA = holes.ToArray();
            solver.setValues(holesA);

            solver.Solve();
        }

        public async Task ConnectTwoPoints(Coordinate coordinateA, Coordinate coordinateB) {
            Debug.WriteLine("Main called");
            await draw.ConnectTwoPoints(coordinateA, coordinateB);
        }
        private void Connect_Click(object sender, RoutedEventArgs e)
        {
            draw.ConnectPoints();
            infoText.Text = "Points Connected.";
        }

        private void ClearLines_Click(object sender, RoutedEventArgs e)
        {
            draw.ClearLines();
            infoText.Text = "Lines Cleared";
        }

        private void ClearCanvas_Click(object sender, RoutedEventArgs e)
        {
            draw.ClearCanvas();
            infoText.Text = ".";
        }

        private void Logic_Click(object sender, RoutedEventArgs e)
        {
            TSPSolver(holes);
        }
    }
}