﻿using Microsoft.Win32;


namespace Visualization_Project
{
    public class FileExplorer
    {
        public static string SelectCsvFile()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog
            {
                Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*",
                Title = "Select CSV File"
            };

            if (openFileDialog.ShowDialog() == true)
            {
                return openFileDialog.FileName;  //Return File path
            }

            return null; // User canceled the selection
        }
    }
}
