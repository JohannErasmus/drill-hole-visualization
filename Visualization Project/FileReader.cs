﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;

namespace Visualization_Project
{
    public class FileReader
    {
        public static List<Coordinate> ReadCoordinates(string filePath)
        {
            List<Coordinate> coordinates = new List<Coordinate>();

            try
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        string[] values = line.Split(',');

                        CultureInfo culture = CultureInfo.InvariantCulture;

                        if (values.Length >= 2 && double.TryParse(values[0], NumberStyles.Float, culture, out double x) && double.TryParse(values[1], NumberStyles.Float, culture, out double y))
                        {
                            coordinates.Add(new Coordinate(x, y));
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                // Handle file reading errors
                Console.WriteLine($"Error reading CSV file: {ex.Message}");
            }

            return coordinates;
        }
    }

    public class Coordinate
    {
        public double X { get; set; }
        public double Y { get; set; }

        public Coordinate(double x, double y)
        {
            X = x;
            Y = y;
        }

        public override string ToString()
        {
            return $"X: {X}, Y: {Y}";
        }
    }
}
